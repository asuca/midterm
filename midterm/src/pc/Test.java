package pc;

public class Test {

	public static void main(String[] args) {
		Dimension dimension = new Dimension(20,20,5);
		Case theCase = new Case("220B","Dell","240",dimension);
		Monitor theMonitor = new Monitor("27inch Bease","Acer",27,new Resolution(2540,1440));
		MotherBoard theMotherboard = new MotherBoard("BJ-200","ASUS",4,6,"V2.44");
		
		PC thePC = new PC(theMonitor,theCase,theMotherboard);
		thePC.getTheCase().pressPowerButton();	
	}
}
