package pc;

public class PC {
	private Monitor theMonitor;
	private Case theCase;
	private MotherBoard theMotherBoard;
	
	PC(Monitor theMonitor,Case theCase,MotherBoard theMotherBoard){
		this.theMonitor = theMonitor;
		this.theCase = theCase;
		this.theMotherBoard = theMotherBoard;	
	}
	void powerOn(String OSName) {
		theCase.pressPowerButton();
		theMotherBoard.powerOn(OSName);
	}
	Monitor getTheMonitor() {
		return theMonitor;
	}
	Case getTheCase() {
		return theCase;
	}
	MotherBoard getTheMotherBoard() {
		return theMotherBoard;
	}
}
