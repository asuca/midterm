package carpetcoscal;

public class Calculator {
	private Floor floor;
	private Carpet carpet;
	Calculator(Floor floor,Carpet carpet){
		this.floor  = floor;
		this.carpet = carpet;
	}
	double getTotalCost() {
		double totalCost = floor.getArea() * carpet.getCost();
		return totalCost;
	}
}
