package carpetcoscal;

public class Floor {
	private double width;
	private double length;
	//constructor
	Floor(double width,double length){
		this.width  = width < 0.0d?0.0d:width;
		this.length = length < 0.0d?0.0d:length;
	}
	//method
	double getArea() {
		return width * length;
	}
}
