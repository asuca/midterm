package carpetcoscal;

public class Carpet {
	private double cost;
	//constructor
	Carpet(double cost){
		this.cost = cost < 0.0d?0.0d:cost;
	}
	//method
	double getCost(){
		return cost;
	}
}
