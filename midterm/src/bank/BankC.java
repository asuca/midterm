package bank;

public class BankC extends Bank {
	private int depMoney;
	BankC(int depMoney) {
		this.depMoney = depMoney;
	}
	@Override
	int getBalance() {
		return depMoney;
	}
}
