package bank;

public class BankA extends Bank {
	private int depMoney;
	
	BankA(int depMoney) {
		this.depMoney = depMoney;
	}
	
	@Override
	int getBalance() {
		return depMoney;
	}
	
}
