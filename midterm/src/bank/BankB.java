package bank;

public class BankB extends Bank {
	private int depMoney;
	BankB(int depMoney) {
		this.depMoney = depMoney;
	}
	@Override
	int getBalance() {
		return depMoney;
	}
	
}
